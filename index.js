//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes");

//Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection
//Connecting to MongoDB Atlas

mongoose.set('strictQuery', true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.w2uuxss.mongodb.net/s35?retryWrites=true&w=majority",
  {
    useNewUrlParser : true,
    useUnifiedTopology: true
  }
);

//connection to database
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))


//Add the task route
//allows all the task routes created in the taskRoutes.js file to use "/tasks" route
app.use("/tasks",taskRoutes)


//Server listening
app.listen(port,()=>console.log(`Now listening to port ${port}`))

/*
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S36.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
12. Send a screen cap of your get and put requests
*/