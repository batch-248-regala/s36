//Controllers contain the functions and business logic of our Express JS application
//Meaning all the operations it can do will be placed in this file

const Task = require("../models/Task");

//Controller function for getting all the tasks
//Define functions to be used in the taskRoutes.js and export these functions

module.exports.getAllTasks = () => {

  //the "return" statement, returns the result of the Mongoose method "find" back to the taskRoutes.js file
  return Task.find({}).then(result=>{
    return result;
  })
}

//Mini Activity Start
// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoutes.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

  // Creates a task object based on the Mongoose model "Task"
  let newTask = new Task({
    
    // Sets the "name" property with the value received from the client/Postman
    name : requestBody.name

  })

  // Saves the newly created "newTask" object in the MongoDB database
  // The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
  // The "then" method will accept the following 2 arguments:
    // The first parameter will store the result returned by the Mongoose "save" method
    // The second parameter will store the "error" object
  // Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
    // Ex.
      // newUser.save((saveErr, savedTask) => {})
  return newTask.save().then((task, error) => {

    // If an error is encountered returns a "false" boolean back to the client/Postman
    if (error) {

      console.log(error);
      // If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
      // Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
      // The else statement will no longer be evaluated
      return false;

    // Save successful, returns the new task object back to the client/Postman
    } else {

      return task; 

    }

  })

}

//Mini Activity End

//Controller function for deleting a task
//"taskId" is the URL parameter pssed from the taskRoutes.js file

//Business Logic
/*
  1. Look for the task with the corresponding id provided in the URL/route
  2. Delete the task using Mongoose Method "findByIdAndRemove" with the smae id provided in the route
*/

module.exports.deleteTask = (taskId) =>{

  return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{

    if(err){

      console.log(err);
      return false

    }else{
      return `${removedTask.name} has been deleted`;
    }
  })
}

//Controller function for updating a task
//Business Logic
/*
  1. Get the task with the id using the Mongoose method "findById"
  2. Replace the task's name returned from the db with the "name" property form the request body
  3. Save the task


*/

module.exports.updateTask = (taskId, newContent) =>{

  return Task.findById(taskId).then((result,error)=>{

    if(error){
      console.log(err);
      return false
    }

    result.name = newContent.name;

    return result.save().then((updatedTask,saveErr)=>{

      if(saveErr){
        console.log(saveErr);
        return false
      }else{
        return updatedTask;
      }
    })
  })
}


// Get a specific task by ID
module.exports.getTaskById = (req, res) => {
  Task.findById(req.params.id).then((result) => {
    res.send(result);
  });
};

module.exports.completeTask = (id) => {
  return Task.findByIdAndUpdate(id, {status: "complete"}, {new: true})
    .then(result => {
      return result;
    });
}


